#StealthGiraffe Theme Plan

##Colour Palette

###Background and foreground
  Background: Dark, translucent, blur
    color0 and bg
    47, 54, 64
  CurrentLine: slightly darker than bg
    35, 40, 48
  Selection: Darker than CurrentLine
    33, 37, 46
  Foreground (i.e. bland text): warm light grey
    Color 7 and foreground
    217, 214, 208

###Functional text
  Comments: Deep muted green
    color4
    52, 82, 47
  Highlight: light muted orange (ensure text in highlights switches to dark)
    246, 163, 107
  Curly braces and encapsulaters: darker pastel orange
    color2
    237, 138, 71
  keywords (return, new etc.): light pastel green
    color1
    78, 120, 71
  keyword types (function, var, this): light blue
    color 6
    95, 174, 194
  defined words (true, false): medium green
    59, 92, 53
  Strings (in quotations): soft yellow
    color3
    232, 197, 100
  error: pastel orangey red
    230, 115, 90
